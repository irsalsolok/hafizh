<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class siswaController extends Controller
{

    public function create()
    {
        return view('siswa.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|unique:siswa',
            'alamat' => 'required',
        ]);
        $query = DB::table('siswa')->insert([
            "nama" => $request["nama"],
            "alamat" => $request["alamat"]
        ]);
        return redirect('/siswa');
    }
    public function index()
    {
        $siswa = DB::table('siswa')->get();
        return view('siswa.index', compact('siswa'));
    }
    public function show($id)
    {
        $siswa = DB::table('siswa')->where('id', $id)->first();
        return view('siswa.show', compact('siswa'));
    }
    public function edit($id)
    {
        $siswa = DB::table('siswa')->where('id', $id)->first();
        return view('siswa.edit', compact('siswa'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required|unique:siswa',
            'alamat' => 'required',
        ]);

        $query = DB::table('siswa')
            ->where('id', $id)
            ->update([
                'nama' => $request["nama"],
                'alamat' => $request["alamat"]
            ]);
        return redirect('/siswa');
    }
    public function destroy($id)
    {
        $query = DB::table('siswa')->where('id', $id)->delete();
        return redirect('/siswa');
    }
}
