<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashbordController;
use App\Http\Controllers\IndexController;


Route::get('/', 'IndexController@home');
Route::get('/registrasi', 'AuthController@registrasi');
Route::get('/welcome', 'AuthController@welcome');
Route::get('/table', 'IndexController@table');

Route::get('/data_tables', 'IndexController@data_tables');
Route::get('/master', function () {
    return view('layout.master');
});

Route::get('/post', 'PostController@index');
Route::get('/post/create', 'PostController@create');
Route::post('/post/', 'PostController@store');
Route::get('/post/{post_id}', 'PostController@show');
Route::get('/post/{post_id}/edit', 'PostController@edit');
Route::put('/post/{post_id}', 'PostController@update');
Route::delete('/post/{post_id}', 'PostController@destroy');


Route::get('/siswa', 'siswaController@index');
Route::get('/siswa/create', 'siswaController@create');
Route::post('/siswa/', 'siswaController@store');
Route::get('/siswa/{siswa_id}', 'siswaController@show');
Route::get('/siswa/{siswa_id}/edit', 'siswaController@edit');
Route::put('/siswa/{siswa_id}', 'siswaController@update');
Route::delete('/siswa/{siswa_id}', 'siswaController@destroy');
