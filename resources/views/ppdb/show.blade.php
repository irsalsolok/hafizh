@extends('layout.master')
@section('judul')
Data Tables
@endsection

@push('script')
<script src="admin/plugins/datatables/jquery.dataTables.js"></script>
<script src="admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
    $(function() {
        $("#example1").DataTable();
    });
</script>
@endpush

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css" />
@endpush

@section('content')

<h2>Show siswa {{$siswa->id}}</h2>
<h4>{{$siswa->title}}</h4>
<p>{{$siswa->body}}</p>

@endsection