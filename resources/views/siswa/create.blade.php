@extends('layout.master')
@section('judul')
Data Tables
@endsection

@push('script')
<script src="admin/plugins/datatables/jquery.dataTables.js"></script>
<script src="admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
    $(function() {
        $("#example1").DataTable();
    });
</script>
@endpush

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css" />
@endpush

@section('content')<div>
    <h2>Tambah Data</h2>
    <form action="/siswa" method="siswa">
        @csrf
        <div class="form-group">
            <label for="nama">Nama Siswa</label>
            <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan nama">
            @error('title')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="hp">Nomor HP Siswa</label>
            <input type="text" class="form-control" name="hp" id="hp" placeholder="Masukkan Nomor Hp">
            @error('title')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="jurusan">Jurusan</label>
            <select class="form-control" id="jurusan">
                <option value="1">Manajemen Informatika</option>
                <option value="2">Manajemen </option>
                <option value="3">Akuntansi</option>
            </select>
        </div>
        <div class="form-group">
            <label for="alamat">Alamat Siswa</label>
            <input type="text" class="form-control" name="alamat" id="alamat" placeholder="Masukkan alamat">
            @error('body')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            @enderror
        </div>

        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="jekel" id="jekel" value="option1">
            <label class="form-check-label" for="jekel">Laki-laki</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="jekel" id="jekel" value="option2">
            <label class="form-check-label" for="jekel">Prempuan</label>
        </div>
        <p>




            <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
</div>
@endsection