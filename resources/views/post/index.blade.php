@extends('layout.master')
@section('judul')
Data Tables
@endsection

@push('script')
<script src="admin/plugins/datatables/jquery.dataTables.js"></script>
<script src="admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
    $(function() {
        $("#example1").DataTable();
    });
</script>
@endpush

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css" />
@endpush

@section('content')
<a href="/post/create" class="btn btn-primary">Tambah</a>
<table class="table">
    <thead class="thead-light">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Title</th>
            <th scope="col">Body</th>
            <th scope="col" style="display: inline">Actions</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($post as $key=>$value)
        <tr>
            <td>{{$key + 1}}</th>
            <td>{{$value->title}}</td>
            <td>{{$value->body}}</td>
            <td>
                <a href="/post/{{$value->id}}" class="btn btn-info">Show</a>
                <a href="/post/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                <form action="/post/{{$value->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <input type="submit" class="btn btn-danger my-1" value="Delete">
                </form>
            </td>
        </tr>
        @empty
        <tr colspan="3">
            <td>No data</td>
        </tr>
        @endforelse
    </tbody>
</table>
@endsection